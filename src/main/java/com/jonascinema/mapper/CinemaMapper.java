package com.jonascinema.mapper;

import java.util.ArrayList;

import com.jonascinema.dao.Cinema;
import com.jonascinema.dto.CinemaDto;

/**
 * Property mapper
 * 
 * @author Jonas Aro
 */
public class CinemaMapper {
	
	/**
	 * Maps the properties from a cinema DTO ito a cinema object.
	 * 
	 * @param cinemaDto - the DTO with its properties to be set to the new cinema DAO.
	 * @return The cinema DAO with the assigned properties from the cinema DTO.
	 */
	public Cinema dtoToDao(CinemaDto cinemaDto) {
		
		Cinema cinema = new Cinema();
		
		cinema.setCapacity(cinemaDto.getCapacity());
		cinema.setCinemaID(cinemaDto.getCapacity());
		cinema.setCinemaName(cinemaDto.getCinemaName());
		cinema.setCinemaNumber(cinemaDto.getCinemaNumber());
		cinema.setMovieShowing(cinemaDto.getMovieShowing());
		
		return cinema;
	}
	
	/**
	 * Maps the properties from a cinema DAO ito a cinema DTO object.
	 * 
	 * @param cinema - the DAO with its properties to be set to the new cinema DTO.
	 * @return The cinema DTO with the assigned properties from the cinema DAO.
	 */
	public CinemaDto daoToDto(Cinema cinema) {
		
		CinemaDto cinemaDto = new CinemaDto();
		
		cinemaDto.setCapacity(cinema.getCapacity());
		cinemaDto.setCinemaID(cinema.getCapacity());
		cinemaDto.setCinemaName(cinema.getCinemaName());
		cinemaDto.setCinemaNumber(cinema.getCinemaNumber());
		cinemaDto.setMovieShowing(cinema.getMovieShowing());
		
		return cinemaDto;
	}
	
	/**
	 * Maps the properties from each cinema DTO into a cinema DAO List.
	 * 
	 * @param cinemaDtoList - the cinema DTO list with the properties to be retrieved and be assigned into DAOs.
	 * @return the cinema DAO List that now contains the properties from the DTOs.
	 */
	public ArrayList<Cinema> dtoListToDaoList(ArrayList<CinemaDto> cinemaDtoList) {
		
		ArrayList<Cinema> cinemaList = new ArrayList<Cinema>();
		cinemaDtoList.forEach(cinemaDto -> cinemaList.add(dtoToDao(cinemaDto)));
		
		return cinemaList;
	}
	
	/**
	 * Maps the properties from each cinema DAO into a cinema DTO List.
	 * 
	 * @param cinemaList - the cinema DAO list with the properties to be retrieved and be assigned into DTOs.
	 * @return the cinema DTO List that now contains the properties from the DAOs.
	 */
	public ArrayList<CinemaDto> daoListToDtoList(ArrayList<Cinema> cinemaList) {
		
		ArrayList<CinemaDto> cinemaDtoList = new ArrayList<CinemaDto>();
		cinemaList.forEach(cinemaDao -> cinemaDtoList.add(daoToDto(cinemaDao)));
		
		return cinemaDtoList;
	}

}
