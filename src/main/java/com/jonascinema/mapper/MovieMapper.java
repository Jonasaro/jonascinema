package com.jonascinema.mapper;

import java.util.ArrayList;

import com.jonascinema.dao.Movie;
import com.jonascinema.dto.MovieDto;

/**
 * Property Mapper for Movie Objects.
 * 
 * @author Jonas Aro
 */
public class MovieMapper {
	
	/**
	 * Maps the properties from a movie DAO ito a movie DTO object.
	 * 
	 * @param movie - the DAO with its properties to be set to the new movie DTO.
	 * @return The movie DTO with the assigned properties from the movie DAO.
	 */
	public MovieDto daoToDto(Movie movie) {

		MovieDto movieDto = new MovieDto();

		movieDto.setAudienceRating(movie.getAudienceRating());
		movieDto.setAuthor(movie.getAuthor());
		movieDto.setEndDate(movie.getEndDate());
		movieDto.setMovieDescription(movie.getMovieDescription());
		movieDto.setMovieId(movie.getMovieId());
		movieDto.setStartDate(movie.getStartDate());
		movieDto.setTitle(movie.getTitle());

		return movieDto;
	}
	
	/**
	 * Maps the properties from a movie DTO ito a movie DAO object.
	 * 
	 * @param movie - the DTO with its properties to be set to the new movie DAO.
	 * @return The movie DAO with the assigned properties from the movie DTO.
	 */
	public Movie dtoToDao(MovieDto movieDto) {
		
		Movie movie = new Movie();
		
		movie.setAudienceRating(movieDto.getAudienceRating());
		movie.setAuthor(movieDto.getAuthor());
		movie.setEndDate(movieDto.getEndDate());
		movie.setMovieDescription(movieDto.getMovieDescription());
		movie.setMovieId(movieDto.getMovieId());
		movie.setStartDate(movieDto.getStartDate());
		movie.setTitle(movieDto.getTitle());

		return movie;
	}
	
	/**
	 * Maps properties from a Movie DTO list into a Movie DAO List.
	 * 
	 * @param movieDtoList - the dto list with the properties from its elements to be mapped.
	 * @return The DAO list with the mapped properties.
	 */
	public ArrayList<Movie> dtoListToDaoList(ArrayList<MovieDto> movieDtoList) {
		
		ArrayList<Movie> movieList = new ArrayList<Movie>();
		movieDtoList.forEach(movieDto -> movieList.add(dtoToDao(movieDto)));
		
		return movieList;
	}
	
	/**
	 * Maps properties from a Movie DAO list into a Movie DTO List.
	 * 
	 * @param movieList - the DAO list with the properties from its elements to be mapped.
	 * @return The DTO list with the mapped properties.
	 */
	public ArrayList<MovieDto> daoListToDtoList(ArrayList<Movie> movieList) {
		
		ArrayList<MovieDto> movieDtoList = new ArrayList<MovieDto>();
		movieList.forEach(movie -> movieDtoList.add(daoToDto(movie)));
		
		return movieDtoList;
	}
	
}
