package com.jonascinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jonascinema.dao.Ticket;

@Repository
public interface JonasTicketRepository extends JpaRepository<Ticket, Long>{

}
