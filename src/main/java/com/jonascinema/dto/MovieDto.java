package com.jonascinema.dto;

import java.util.Date;

/**
 * Class for movie objects.
 * 
 * @author Jonas Aro
 */
public class MovieDto {
	
	private int movieId;

	private String title;
	private String audienceRating;
	private String movieDescription;
	private String author;
	
	private Date startDate;
	private Date endDate;
	
	public int getMovieId() {
		return movieId;
	}

	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAudienceRating() {
		return audienceRating;
	}
	
	public void setAudienceRating(String audienceRating) {
		this.audienceRating = audienceRating;
	}
	
	public String getMovieDescription() {
		return movieDescription;
	}
	
	public void setMovieDescription(String movieDescription) {
		this.movieDescription = movieDescription;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((audienceRating == null) ? 0 : audienceRating.hashCode());
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((movieDescription == null) ? 0 : movieDescription.hashCode());
		result = prime * result + movieId;
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovieDto other = (MovieDto) obj;
		if (audienceRating == null) {
			if (other.audienceRating != null)
				return false;
		} else if (!audienceRating.equals(other.audienceRating))
			return false;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (movieDescription == null) {
			if (other.movieDescription != null)
				return false;
		} else if (!movieDescription.equals(other.movieDescription))
			return false;
		if (movieId != other.movieId)
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
}
