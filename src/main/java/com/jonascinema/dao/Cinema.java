package com.jonascinema.dao;

/**
 * Class for cinema objects.
 * 
 * @author Jonas Aro
 */
public class Cinema {
	
	private int cinemaID;
	private int capacity;
	private int cinemaNumber;
	
	private String cinemaName;
	private String movieShowing;

	public int getCinemaID() {
		return cinemaID;
	}
	
	public void setCinemaID(int cinemaID) {
		this.cinemaID = cinemaID;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	public int getCinemaNumber() {
		return cinemaNumber;
	}
	
	public void setCinemaNumber(int cinemaNumber) {
		this.cinemaNumber = cinemaNumber;
	}
	
	public String getCinemaName() {
		return cinemaName;
	}
	
	public void setCinemaName(String cinemaName) {
		this.cinemaName = cinemaName;
	}
	
	public String getMovieShowing() {
		return movieShowing;
	}
	
	public void setMovieShowing(String movieShowing) {
		this.movieShowing = movieShowing;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + capacity;
		result = prime * result + cinemaID;
		result = prime * result + ((cinemaName == null) ? 0 : cinemaName.hashCode());
		result = prime * result + cinemaNumber;
		result = prime * result + ((movieShowing == null) ? 0 : movieShowing.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cinema other = (Cinema) obj;
		if (capacity != other.capacity)
			return false;
		if (cinemaID != other.cinemaID)
			return false;
		if (cinemaName == null) {
			if (other.cinemaName != null)
				return false;
		} else if (!cinemaName.equals(other.cinemaName))
			return false;
		if (cinemaNumber != other.cinemaNumber)
			return false;
		if (movieShowing == null) {
			if (other.movieShowing != null)
				return false;
		} else if (!movieShowing.equals(other.movieShowing))
			return false;
		return true;
	}
	
}
